<?php


class Content
{
    private $_idContent;
    private $_name;
    private $_descript;
    private $_imageUrl;
    private $_url;
    private $_category;

    function __construct($_descript,$_idContent,$_imageUrl,$_name,$_url,$_category){
        $this->_idContent = $_idContent;
        $this->_descript = $_descript;
        $this->_imageUrl = $_imageUrl;
        $this->_name = $_name;
        $this->_url = $_url;
        $this->_category = $_category;
    }


    public function getIdContent()
    {
        return $this->_idContent;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getDescript()
    {
        return $this->_descript;
    }

    public function getImageUrl()
    {
        return $this->_imageUrl;
    }

    public function  getUrl(){
        return $this->_url;
    }

    public function getCategory(){
        return $this->_category;
    }
}