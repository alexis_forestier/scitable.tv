<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.php");

if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $password = $_POST['password'];

    $user = $connect->query('SELECT *
                            FROM Users 
                            WHERE email = '. $connect->quote($_POST['email']))->fetch();


    if (!empty($user) && password_verify($password, $user['password'])) {

        unset($user['password']);
        unset($user['active']);

        $_SESSION['user'] = $user;

        if (isset($_POST['remember'])) {
            setcookie('user', $user['email'], time() + 172800, "/");
        }

        header('Location: /home.php');
        exit();
    }
}

header('Location: /index.php');
exit();
