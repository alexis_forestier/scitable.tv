<?php
include 'includes/config.php';


try{
    $stmt = $connect->prepare($sql = "SELECT * FROM Content");
    $stmt->execute();
    $result = $stmt->fetchAll();
    
}catch (PDOException $e) {
    echo 'Erreur sql : ' . $e->getMessage();
}

$total = count($result);


function sourceYoutube($_imageUrl){
    if (strpos($_imageUrl, "http://img.youtube.com/vi/") === false) {
        return "img/";
    }
    return "";
}

function createDivContent($_idContent, $_name, $_descript, $_imageUrl,$_category){

    return '<div class=contentHome id = content-' . $_idContent . '> 
                <div> ' . $_name .' </div>
                <div> '. $_category .' </div>
                <a href="video.php?video='.$_idContent .'"> 
                    <div> <img src="'. sourceYoutube($_imageUrl) .$_imageUrl . '"  alt="contenu- . '. $_idContent . ' "></div>
                </a>
                
                <div> ' . $_descript .' </div>
            </div>';

}

function createAllContent($connect,$_sql){
    $tabContent = array();

    
    try {

        $stmt = $connect->prepare($_sql);
        $stmt->execute();
        $result = $stmt->fetchAll();


        if(isset($_GET['num']) && $_GET['num']*6 < count($result) && $_GET['num']*6 +6 > count($result) ){
            
            
            for($count = $_GET['num']*6; $count < count($result); $count++){

                $content = new Content($result[$count]['descript'],$result[$count]['idContent']
                    ,$result[$count]['imageUrl'],$result[$count]['name'],$result[$count]['videoUrl']
                    ,$result[$count]['category']);
                array_push($tabContent,$content);
    
            }
        }

        else if(isset($_GET['num'])){
            
            for($count = $_GET['num']*6; $count < $_GET['num']*6+6; $count++){

                $content = new Content($result[$count]['descript'],$result[$count]['idContent']
                    ,$result[$count]['imageUrl'],$result[$count]['name'],$result[$count]['videoUrl']
                    ,$result[$count]['category']);
                array_push($tabContent,$content);
    
            }
        }

        else if(!isset($_GET['search']) && !isset($_GET['num']) && !isset($_GET['category'])){
            for($count = 0; $count < 6; $count++){

                $content = new Content($result[$count]['descript'],$result[$count]['idContent']
                    ,$result[$count]['imageUrl'],$result[$count]['name'],$result[$count]['videoUrl']
                    ,$result[$count]['category']);
                array_push($tabContent,$content);
    
            }
        }

        else{
            
            for($count = 0; $count < count($result); $count++){

                $content = new Content($result[$count]['descript'],$result[$count]['idContent']
                    ,$result[$count]['imageUrl'],$result[$count]['name'],$result[$count]['videoUrl']
                    ,$result[$count]['category']);
                array_push($tabContent,$content);
    
            }
        }
        
       
        

    } catch (PDOException $e) {
        echo 'Erreur sql : ' . $e->getMessage();
    }


    $counter = 0;
    for($counter ; $counter < count($tabContent); $counter++){
        echo createDivContent($tabContent[$counter]->getIdContent(),$tabContent[$counter]->getName(),
            $tabContent[$counter]->getDescript(),$tabContent[$counter]->getImageUrl()
            ,$tabContent[$counter]->getCategory());
    }

}


?>
<!DOCTYPE html>
<html lang="fr">

<?php
$pageName = "Scitable.TV - HOME";
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/head.php"); 
?>

<body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/header.php"); ?>

    <main class="main" id="home-page">


        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/aside.php"); ?>


        <h1><?php 
            if(empty($_GET['category']))
                if(empty($_GET['search'])){
                    echo "CURRENT TRENDS";
                }
                else{
                    echo "SEARCH RESULTS";
                }
                

            else
                echo "Video de " . $_GET['category'];

            ;?></h1>



        <section>

            <div id="contenu">
                <?php 
                
                
                if(!isset($_GET['category'])&&empty($_GET['category'])){
                    if(!isset($_GET['search'])&&empty($_GET['search'])){
                        $sql = "SELECT * FROM Content";
                    }  
                    else{
                        $sql = 'SELECT * 
                        FROM Content 
                        WHERE lower(name) LIKE "%'.strtolower($_GET['search']).'%" 
                        OR lower(category) LIKE "%'.strtolower($_GET['search']).'%";';
                    }
                }  
                else{
                    $sql = 'SELECT * FROM Content WHERE category LIKE "%'.$_GET['category'] .'%"';
                }

                createAllContent($connect, $sql);
                ?>

            </div>

        </section>

        <div id="fleche-droite">
            <?php 
            
            if(!isset($_GET['search']) && !isset($_GET['category'])){

                if(isset($_GET['num'])){
                    $words = explode("=",$_SERVER['REQUEST_URI']);
                    if($words[1]==0){
                        $num = 0;
                    }
                    else{
                        $num = $words[1]-1;
                    }
                    
                    echo '<a href="home.php?num='.$num .'"> <img src="img/gauche.png" alt="suivant"></a>';
                }
                else{
                    echo ' <img src="img/gauche.png" alt="suivant">';
                }
            }

           
            ?>
            
        </div>

        <div id="fleche-gauche">
            <?php 
            if(!isset($_GET['search']) && !isset($_GET['category'])){

                
                if(isset($_GET['num']) && $_GET['num']*6 < count($result) && $_GET['num']*6 +6 >= count($result) ){
                    //echo '<a href="home.php?num='. floor(count($result)/6) .'"> <img src="img/droite.png" alt="suivant"></a>';
                    echo ' <img src="img/droite.png" alt="suivant"></a>';
                }

                else if(isset($_GET['num'])){
                    $words = explode("=",$_SERVER['REQUEST_URI']);
                
                    $num = $words[1]+1;
                
                    echo '<a href="home.php?num='.$num .'"> <img src="img/droite.png" alt="suivant"></a>';
                }
                else{
                    echo '<a href="home.php?num=1"> <img src="img/droite.png" alt="suivant"></a>';
                }
                

            }
            ?>
        </div>

    </main>


    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/footer.php"); ?>

    <script src="js/action.js"></script>
</body>

</html>