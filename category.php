<?php
include 'includes/config.php';


$tabCategory = array();
$isInTab = 0;

$cpt =0;



try {

    $stmt = $connect->prepare("SELECT * FROM Content");
    $stmt->execute();
    $result = $stmt->fetchAll();

    $init = explode('/',$result[0]['category']); //On initialise le tableau de catégorie
    array_push($tabCategory,$init[0]);

    //ON CHERCHE TOUTES LES CATEGORIE EXISTANTE DANS LA BDD ICI -->

    for($i = 0; $i < count($result); $i++){

        $words = explode('/',$result[$i]['category']);

        for($a = 0; $a < count($words); $a++){//Cette boucle vas vérifier toute les catégorie d'un film

            while($cpt<count($tabCategory)){ //Cette boucle prend une catégorie et la vérifie dans le tableau de catégorie

                if($words[$a]==$tabCategory[$cpt]){
                    $isInTab = 0;
                    break;
                }
                else{
                    $isInTab=1;
                }
                $cpt++;
            }
            $cpt=0;

            if($isInTab==1)
                array_push($tabCategory,$words[$a]);

        }
    }



} catch (PDOException $e) {
    echo 'Erreur sql : ' . $e->getMessage();
}


function sourceYoutube($_imageUrl){
    if (strpos($_imageUrl, "http://img.youtube.com/vi/") === false) {
        return "img/";
    }
    return "";
}

function addCategory($_name,$conn){

    try {

        $stmt = $conn->prepare("SELECT * FROM Content");
        $stmt->execute();
        $res = $stmt->fetchAll();


    } catch (PDOException $e) {
        echo 'Erreur sql : ' . $e->getMessage();
    }


    $ret = '<div class="category-content">
                <div>
                    <h2>'. $_name .'</h2>
                    <a href="/home.php?category='.$_name.'">Voir tout </a>
                    <div>';


    for($i = 0; $i < count($res);$i++){

        if(isCategory($res[$i]['category'],$_name)){

            $ret.= '<div>
                    <div> ' . $res[$i]['name'] .' </div>
                    <div> '. $res[$i]['category'] .' </div>
                    <a href="video.php?video='.$res[$i]['idContent'] .'"> 
                        <div> <img src="'. sourceYoutube($res[$i]['imageUrl']) .$res[$i]['imageUrl'] . '"  alt="contenu- . '. $res[$i]['idContent'] . ' "></div>
                    </a>
                    
                 </div>';
        }
    }



    $ret .= '       </div>
                </div>
            </div>';

    return $ret;

}

function addAllCategory($conn, array $tabCate){



    $ret='';
    for($i = 0; $i < count($tabCate); $i++){
        $ret.= addCategory($tabCate[$i],$conn);
    }
    return $ret;
}

function isCategory($_categorys, $_categoryToFind){
    $isTrue = 0;
    $words = explode('/',$_categorys);
    for($i = 0; $i<count($words);$i++){
        if($words[$i]==$_categoryToFind){
            $isTrue=1;
        }
    }
    return $isTrue;
}


?>

<!DOCTYPE html>
<html lang="fr">

<?php $pageName = "Scitable.TV - CATEGORY";
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/head.php"); ?>

<body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/header.php"); ?>

    <main class="main" id="category-page">
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/aside.php"); ?>

        <h1>ALL CATEGORY</h1>


        <section>

            <div id="category-listings">

                <?= addAllCategory($connect, $tabCategory); ?>

            </div>

        </section>

    </main>


    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/footer.php"); ?>

    <script src="js/action.js"></script>
</body>

</html>