# SCITABLE.TV

Petit projet de 2ᵉ année de DUT **Site Vidéo**

Plus tard, nommé SCITABLE.TV en référence au site SeaTable.
Le but de ce projet est de créer un site d'hébergement de vidéos ou de films
où les utilisateurs pourraient donner une note.
Les utilisateurs administrateur peuvent ajouter et supprimer les vidéos/films du site et également gérer les utilisateurs non-admin.

**Tâche inachevée** : Un programme Python devait gérer le système de recommandation du site.

En collaboration avec @durairajuprawinraj01, LE NORMAND ELIOTT, @hedi_zair

## Tester le projet

1. Cloner le projet 
2. Télécharger et intégrer la [base de données](https://drive.google.com/file/d/15kOlkS0r_nKB7F2FgMVZcFszciaiVTtx/view?usp=sharing) à l'extérieur du répertoire du projet
    - ![](https://cdn.discordapp.com/attachments/804441909891956807/928356993964605470/doc2.png)
3. Placer vous dans le répertoire du projet cloner et dans la barre de chemin d'accès taper **"cmd"**
Un terminal s'ouvre, entrer la commande suivante :

```
php -S localhost:80
```

4. Laisser le terminal ouvert et dans la barre de recherche d'un navigateur internet (Chrome, Firefox, Opera...)
veuillez taper **"localhost:80"**.
Vous devriez accéder à l'index du projet.

## Diagramme de classes

Voici l'organisation de la base de données

![](https://cdn.discordapp.com/attachments/804441909891956807/928354904672710696/ddc.png)

## Données du projet GitLab original

Projet original disponible sur [GitLab Forge](https://forge.univ-lyon1.fr/).

Le projet original est privé.

| Commits | 90 |
|---|---|
| Branches | 4 |


