<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.php");
if (isset($_SESSION['user'])) {
    header('Location: /home.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="fr">

<?php 
$pageName = "Connecter Vous...";
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/head.php");
?>


<body>

    <main id="index-page">

        <div>
            <img src="img/Logo_SeatableTV.png" alt="logo">
            <h1>SCITABLE.TV</h1>
        </div>

        <section>

            <form id="form-signin" class="form" action="/signin.php" method="post">

                <h2>Inscription</h2>

                <div>
                    <label for="fname1">Prénom</label>
                    <input id="fname1" type="text" name="fname">
                </div>

                <div>
                    <label for="lname1">Nom</label>
                    <input id="lname1" type="text" name="lname">
                </div>

                <div>
                    <label for="email1">Email</label>
                    <input id="email1" type="email" name="email">
                </div>

                <div>
                    <label for="password1">Mot de passe</label>
                    <input id="password1" type="password" name="password">
                </div>

                <div>
                    <label for="repeat-password">Répéter mot de passe</label>
                    <input id="repeat-password" type="password" name="repeat-password">
                </div>

                <button id="b-sign-in" type="submit">S'inscrire</button>

            </form>

            <form id="form-login" class="form" action="/login.php" method="post">

                <h2>Connexion</h2>

                <div>
                    <label for="email2">Email</label>
                    <input id="email2" type="email" name="email">
                </div>

                <div>
                    <label for="password2">Mot de passe</label>
                    <input id="password2" type="password" name="password">
                </div>

                <label> 
                        <input type="checkbox" name="remember">
                        Se souvenir de moi
                </label>


                <button id="b-login" type="submit">Se connecter</button>

            </form>

        </section>

    </main>


</body>

</html>