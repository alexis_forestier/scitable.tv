<?php
if (!empty($_POST) && isset($_POST['url'])) {
    $date = date('YmdHis');
    $date = date('Y-m-d H:i:s', strtotime($date));
    if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
        $type = explode('/', $_FILES["image"]['type'])[1];
        move_uploaded_file($_FILES["image"]['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/img/image" . $date . "." . $type);
        $image = "image" . $date;
    }
    if ($_POST['source'] == "youtube") {
        $videoID = substr($_POST['url'], 32, 11);
        echo $videoID;
        if (!isset($image)) {
            $image = 'http://img.youtube.com/vi/' . $videoID . '/hqdefault.jpg';
        }
    }

    $connect->beginTransaction();
    try {
        $sth = $connect->prepare("INSERT INTO Content (name,descript,imageUrl,videoUrl,updated,category)
                                        VALUES (:name,:descript,:imageUrl,:videoUrl,:updated,:category);");
        $sth->execute(
            [
                'name' => $_POST['title'],
                'descript' => $_POST['desc'],
                'imageUrl' => $image??NULL,
                'videoUrl' => $videoID,
                'updated' => $date,
                'category' => $_POST['categories'],
            ]
        );
        $connect->commit();
        $_SESSION['video'] = true;
    } catch (\Throwable $th) {
        $connect->rollBack();
        $_SESSION['video'] = false;
    }
}
?>

<div id="back-title">
    <a href="/admin.php">retour</a>
    <h1>Ajouter une vidéo</h1>
</div>

<form id="addVideo" method="post" enctype="multipart/form-data">
    <label for="title">Titre
        <input type="text" id="title" name="title" required>
    </label>
    <label for="desc">Description
        <textarea id="desc" name="desc"></textarea>
    </label>
    <label for="desc">Source
        <select name="source" id="source">
            <option value="others" selected>autres</option>
            <option value="youtube">youtube</option>
        </select>
    </label>
    <div id="sourceForm">

    </div>
    <label for="categories">Categories
        <input type="text" id="categories" name="categories">
    </label>
    <p>Séparer les catégories avec un "/"</p>
    <button type="submit">Ajouter</button>
</form>

<script>
    let source = document.getElementById('source');

    function changeSourceForm() {
        switch (source.value) {
            case "youtube": //récupère la miniature de la vidéo youtube
                document.getElementById('sourceForm').innerHTML = '<label for="url">Url de la video' +
                    '<input type="text" id="url" name="url" required>' +
                    '</label>' +
                    '<p>le lien doit être de la forme : "https://www.youtube.com/watch?v=..."</p>';
                break;

            case "others": //miniature personnalisée
                document.getElementById('sourceForm').innerHTML = '<label for="image">Miniature' +
                    '<input type="file" id="image" name="image">' +
                    '</label>' +
                    '<label for="url">Url de la video' +
                    '<input type="text" id="url" name="url" required>' +
                    '</label>';
                break;
        }
    }
    changeSourceForm();
    source.addEventListener('change', changeSourceForm);
</script>