<?php

function itsYou($id){
    if($id == $_SESSION['user']['idUsers']){
        return 'you';
    }
}

function disabled($id){
    if($id == $_SESSION['user']['idUsers']){
        return 'disabled';
    }
}

function viewUserList($connect)
{
    $users = $connect->query("SELECT idUsers,name,lastname,imageUrl,active,admin FROM Users")->fetchAll();

    if ($users) {
        $string = "";
        foreach ($users as $user) {
            $string .= '<div class="user">
                        <div>
                            <img src="/img/' . $user['imageUrl'] . '" alt="image">
                        </div>
                        <p class="' . itsYou($user['idUsers']) . '">' . $user['name'] . ' ' . $user['lastname'] . '</p>
                        <input id="h-blocked-' . $user['idUsers'] . '" type="hidden" name="active" value="' . $user['active'] . '">
                        <input id="h-admin-' . $user['idUsers'] . '" type="hidden" name="admin" value="' . $user['admin'] . '">
                        <button id="admin-' . $user['idUsers'] . '" type="button" '.disabled($user['idUsers']).'>'.($user['admin']?"unadmin":"admin").'</button>
                        <button id="blocked-' . $user['idUsers'] . '" type="button" '.disabled($user['idUsers']).'>'.($user['active']?"block":"unblock").'</button>
                    </div>';
        }
        
        return $string;
    }
    return "Il semble qu'il n'y ai aucun utilisateur dans notre base de données";
}

?>

<div id="back-title">
    <a href="/admin.php">retour</a>
    <h1>Liste des utilisateurs</h1>
</div>

<form>
    <?= viewUserList($connect) ?>
</form>

<script>
    let users = document.querySelectorAll('.user > button');

    function modifyDATA() {
        let id_splited = this.id.split('-');
        let value = document.getElementById('h-' + id_splited[0] + '-' + id_splited[1]).value
        const xhttp = new XMLHttpRequest();
        xhttp.onload = function() {
            document.getElementById(id_splited[0]+"-"+id_splited[1]).textContent = this.responseText;
            if(document.getElementById('h-' + id_splited[0] + '-' + id_splited[1]).value == 0){
                document.getElementById('h-' + id_splited[0] + '-' + id_splited[1]).value = 1
            }else{
                document.getElementById('h-' + id_splited[0] + '-' + id_splited[1]).value = 0
            }
        }
        xhttp.open("POST", "/sendDATA.php");
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send('id=' + id_splited[1] + '&' + id_splited[0] + '=' + value);
    }

    for (let index = 0; index < users.length; index++) {
        users[index].addEventListener('click', modifyDATA);
    }
</script>