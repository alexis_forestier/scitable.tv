<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.php");

if(!isset($_SESSION['user']) || empty($_SESSION['user'])){
    header('Location: /index.php');
    exit();
}

try {

    //!!! CODE GIGA LOURD !!!
    $resultat = $connect->query('SELECT * FROM Content WHERE idContent=' . $_GET['video'])->fetch();
    $note = $connect->query('SELECT value 
                            FROM Rate 
                            WHERE idContent =' . $_GET['video'] . ' AND idUsers = ' . $_SESSION['user']['idUsers'])->fetchColumn();
    $noteAVG = $connect->query('SELECT avg(value) FROM Rate WHERE idContent = '.$_GET['video'])->fetchColumn();

} catch (PDOException $e) {
    //echo 'Erreur sql : ' . $e->getMessage();
}

if(!$resultat){
    header('Location: /home.php');
    exit();
}

function addVideo($_url)
{
    return '<iframe width="848" height="477" src=https://www.youtube.com/embed/' . $_url . ' 
        title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    ';
}

?>

<!DOCTYPE html>
<html lang="fr">

<?php
$pageName = "Scitable.TV - " . $resultat['name'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/head.php");
?>


<body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/header.php"); ?>

    <main id="main-video">

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/aside.php"); ?>

        <div>
            <h2> <?= $resultat['name'] ?> </h2>
        </div>


        <div>
            <div id="lecteur-video">

                <?= addVideo($resultat['videoUrl']); ?>

            </div>

            <div id="barre-under">

                <div id="note">
                    <p id="avg">Note moyenne : <?= $noteAVG ?></p>
                    <p id="yourNote">Votre note : <?= $note ?></p>
                </div>

                <div id="divRange">
                    <input class="rating" id="rangeNote" type="range" min="0" max="5" value="<?= $note ?>" step="0.5" style="--value:<?= $note ?>">
                </div>

            </div>

        </div>

        <div id="description">

            <h3>DESCRIPTION</h3>

            <p>
                <?php echo $resultat['descript'] ?>
            </p>

        </div>


    </main>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/footer.php"); ?>

</body>
<script>
    let range = document.getElementById('rangeNote');

    function sendNote(){
        let note = range.value;
        const xhttp = new XMLHttpRequest();
        xhttp.onload = function() {
            document.getElementById('yourNote').innerHTML = 'Votre note : '+this.responseText.split('-')[0];
            document.getElementById('avg').innerHTML = 'Note moyenne : '+this.responseText.split('-')[1];
        }
        xhttp.open("POST", "/sendNote.php");
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send('uid=' + <?=$_SESSION['user']['idUsers'] ?> + '&vid=' + <?=$_GET['video'] ?> + '&note=' + note);
    }

    range.addEventListener('change', sendNote);
    range.addEventListener('input',function(){this.style.setProperty('--value', `${this.valueAsNumber}`)});
</script>
<script src="js/action.js"></script>

</html>